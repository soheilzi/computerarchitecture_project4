module TBDataPath();
  reg clk;
  reg rst;
  reg RegDst;
  reg SelPC4;
  reg Sel31w;
  reg ALUsrc;
  reg RegWrite;
  reg MemRead;
  reg MemWrite;
  reg MemtoReg;
  reg SelPC;
  reg PCb;
  reg PCjal;
  reg PCjr;
  reg [2:0] ALUoperation;
  wire Zero;
  wire [5:0] opc;
  wire [5:0] func;
  
  DataPath U1 (clk, rst, RegDst, SelPC4, Sel31w, ALUsrc, RegWrite, MemRead, MemWrite, MemtoReg, SelPC, PCb, PCjal, PCjr, ALUoperation, zero, opc, func);
    
  initial begin
    clk = 0;
    #500 clk = 1;
    #500 clk = 0;
    #500
    $stop;
  end
endmodule  
  
  
  

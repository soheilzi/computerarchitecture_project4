module Instruction_memory(input mem_read, mem_write, clk, rst, input [31:0] address, input[31:0] write_data, output reg [31:0] instruction);
    reg [7:0] mem [0:2047];
    integer i;
    initial begin
        $readmemb("instruction_memory.list", mem);
    end
    always@(posedge clk, posedge rst) begin
        if(rst == 1) begin
            $readmemb("instruction_memory.list", mem);            
        end
        else begin
            if(mem_write == 1) begin
                for(i = 0; i < 4; i = i + 1) begin
                    mem[address + i] = write_data[24 - i * 8 +: 8];
                end
            end
        end
    end
    always@(address, mem_read, mem)begin
        instruction = 32'b0;
        if(mem_read == 1) begin
            instruction = {mem[address], mem[address + 1], mem[address + 2], mem[address + 3]};
        end
    end
endmodule
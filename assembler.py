opc = {"RT":"000000", "LW":"100011", "SW":"101011", "BEQ":"000100", "BNQ":"000101", "ADDI":"001000", "ANDI":"001100", "JU":"000010", "JAL":"000011", "JR":"100000"}
func = {"ADD":"100000", "SUB":"100010", "OR":"100101", "AND":"100100", "SLT":"101010"}
# get-content .\command.txt | python .\assembler.py
while True:
        try:
            s=input()
            command = input()
            out = ""
            key_len = 0
            for key in opc:
                if command.find(key) != -1:
                    out += opc[key]
                    key_len = len(key)
            command = command[key_len:]
            for ch in command:
                if ch == "0" or ch == "1":
                    out += ch
            for key in func:
                if command.find(key) != -1:
                    out += func[key]
            print(out[0:8])
            print(out[8:16])
            print(out[16:24])
            print(out[24:32])
        except EOFError:
            break
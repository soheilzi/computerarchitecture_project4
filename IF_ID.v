module IF_ID(input clk, rst, IF_Flush, IF_ID_write,
    input [31:0] Inst_in, PC_in,
    output reg [31:0] Inst_out, PC_out);
    always @ (posedge clk, posedge rst) begin
      if((IF_Flush == 1 && IF_ID_write == 1) || rst == 1)
        {Inst_out, PC_out} = 64'b0;
      else if(IF_ID_write == 1) begin
        Inst_out = Inst_in;
        PC_out = PC_in;
      end
    end


endmodule

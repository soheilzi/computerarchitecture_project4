module ForwardingUnit(input EX_MEM_RegWrite, MEM_WB_RegWrite, input [4:0] EX_MEM_Rd, MEM_WB_Rd, ID_EX_Rs, ID_EX_Rt,
    output [1:0]ForwardA, ForwardB);
    wire w1;
    wire w2;
    wire w3;
    wire w4;
    wire w5;
    wire w6;
    assign w1 = EX_MEM_Rd == ID_EX_Rs ? 1 : 0;
    assign w2 = EX_MEM_Rd == ID_EX_Rt ? 1 : 0;
    assign w3 = EX_MEM_Rd == 5'b 0 ? 0 : 1;
    assign w4 = MEM_WB_Rd == ID_EX_Rs ? 1 : 0;
    assign w5 = MEM_WB_Rd == ID_EX_Rt ? 1 : 0;
    assign w6 = MEM_WB_Rd == 5'b 0 ? 0 : 1;

    assign ForwardA[0] = EX_MEM_RegWrite & w1 & w3;
    assign ForwardB[0] = EX_MEM_RegWrite & w2 & w3;
    assign ForwardA[1] = MEM_WB_RegWrite & w4 & w6 & !ForwardA[0];
    assign ForwardB[1] = MEM_WB_RegWrite & w5 & w6 & !ForwardB[0];


endmodule
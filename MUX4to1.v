module MUX4to1(input [31:0]A0, A1, A2, A3, input [1:0]sel, output [31:0] out);
  assign out = (sel == 2'b00) ? A0 : (sel == 2'b01 ? A1 : (sel == 2'b10 ? A2 : (sel == 2'b11 ? A3 : 32'b0)));
endmodule

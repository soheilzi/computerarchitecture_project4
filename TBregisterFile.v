module TBregisterFile();
    reg [31:0] write_data;
    reg [4:0] write_register;
    reg [4:0] read_register1;
    reg [4:0] read_register2;
    reg clk;
    reg rst;
    reg reg_write;

    wire [31:0] read_data1;
    wire [31:0] read_data2;
    
    Register_file cut(.clk(clk), .rst(rst), .write_register(write_register), .read_register1(read_register1), .read_register2(read_register2),
                     .reg_write(reg_write), .read_data1(read_data1), .read_data2(read_data2), .write_data(write_data));
    initial begin
        rst = 1'b1;
        clk = 1'b0;
        #100
        rst = 1'b0;
        repeat(100) #50 clk = ~clk;
    end

    initial begin
        #220
        read_register1 = 5'd4;
        read_register2 = 5'd9;
        write_register = 5'd5;
        write_data = 32'd18;
        reg_write = 1'b1;
        #1000
        write_register = 5'd7;
        write_data = 32'd7;
        reg_write = 1'd1;
        #1000
        reg_write = 1'b0;
        read_register1 = 5'd5;
        read_register2 = 5'd7;
        #1000$stop;
    end
endmodule
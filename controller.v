module Controller(input [5:0]OPC, input [5:0]func, input zero, NoP, output reg [2:0] ALUoperation, 
                    output reg regDst, selPC4, sel31w, ALUsrc, regWrite, memRead, memWrite, memToReg,
                    output reg selPC, PCb, PCjal, PCjr);
    reg [2:0]PCsrc;
    reg [1:0]ALUop;
    wire noOperation;
    assign noOperation = (func == 6'b0 && OPC == 6'b0) ? 0:1;

    parameter [1:0] ADD = 0;
    parameter [1:0] SUB = 1;
    parameter [1:0] RT = 2;
    parameter [1:0] AND = 3;

    // parameter [5:0] OP_RT = 0;
    // parameter [5:0] OP_LW = 1;
    // parameter [5:0] OP_SW = 2;
    // parameter [5:0] OP_BEQ = 3;
    // parameter [5:0] OP_BNQ = 4;
    // parameter [5:0] OP_ADDI = 5;
    // parameter [5:0] OP_ANDI = 6;
    // parameter [5:0] OP_J = 7;
    // parameter [5:0] OP_JAL = 8;
    // parameter [5:0] OP_JR = 9;

    parameter[2:0] SELPC = 0;
    parameter[2:0] PCBEQ = 1;
    parameter[2:0] PCBNQ = 2;
    parameter[2:0] PCJAL = 3;
    parameter[2:0] PCJR = 4;

    always@(negedge NoP, negedge noOperation)begin
        {regDst, selPC4, sel31w, ALUsrc, regWrite, memRead, memWrite, memToReg} = 8'b0;
        ALUop = RT;
        PCsrc = SELPC;
        $display("NoP activated!");
    end

    always@(OPC, zero, posedge NoP, posedge noOperation) begin
        {regDst, selPC4, sel31w, ALUsrc, regWrite, memRead, memWrite, memToReg} = 8'b0;
        ALUop = RT;
        PCsrc = SELPC;
        case (OPC)
            6'b000000: begin // RT
                regDst = 1'b1;
                selPC4 = 1'b0;
                sel31w = 1'b0;
                ALUsrc = 1'b0;
                ALUop = RT;
                regWrite = 1'b1;
                memRead = 1'b0;
                memWrite = 1'b0;
                memToReg = 1'b0;
                PCsrc = SELPC;
            end
            6'b100011: begin // LW
                regDst = 1'b0;
                selPC4 = 1'b0;
                sel31w = 1'b0;
                ALUsrc = 1'b1;
                ALUop = ADD;
                regWrite = 1'b1;
                memRead = 1'b1;
                memWrite = 1'b0;
                memToReg = 1'b1;
                PCsrc = SELPC;
                $display("LW found");
            end
            6'b101011: begin // SW
                // regDst = 1'b;
                // selPC4 = 1'b;
                // sel31w = 1'b;
                ALUsrc = 1'b1;
                ALUop = ADD;
                regWrite = 1'b0;
                memRead = 1'b0;
                memWrite = 1'b1;
                // memToReg = 1'b;
                PCsrc = SELPC;
                $display("SW found");
            end
            6'b000100: begin // BEQ
                // regDst = 1'b;
                // selPC4 = 1'b;
                // sel31w = 1'b;
                ALUsrc = 1'b0;
                ALUop = SUB;
                regWrite = 1'b0;
                memRead = 1'b0;
                memWrite = 1'b0;
                // memToReg = 1'b;
                PCsrc = PCBEQ;
            end
            6'b000101: begin // BNQ
                // regDst = 1'b;
                // selPC4 = 1'b;
                // sel31w = 1'b;
                ALUsrc = 1'b0;
                ALUop = SUB;
                regWrite = 1'b0;
                memRead = 1'b0;
                memWrite = 1'b0;
                // memToReg = 1'b;
                PCsrc = PCBNQ;
            end
            6'b001000: begin // ADDI
                regDst = 1'b0;
                selPC4 = 1'b0;
                sel31w = 1'b0;
                ALUsrc = 1'b1;
                ALUop = ADD;
                regWrite = 1'b1;
                memRead = 1'b0;
                memWrite = 1'b0;
                memToReg = 1'b0;
                PCsrc = SELPC;
            end
            6'b001100: begin // ANDI
                regDst = 1'b0;
                selPC4 = 1'b0;
                sel31w = 1'b0;
                ALUsrc = 1'b1;
                ALUop = AND;
                regWrite = 1'b1;
                memRead = 1'b0;
                memWrite = 1'b0;
                memToReg = 1'b0;
                PCsrc = SELPC;
            end
            6'b000010: begin // J
                // regDst = 1'b;
                // selPC4 = 1'b;
                // sel31w = 1'b;
                // ALUsrc = 1'b;
                // ALUop = 3'b;
                regWrite = 1'b0;
                memRead = 1'b0;
                memWrite = 1'b0;
                // memToReg = 1'b;
                PCsrc = PCJAL;
            end
            6'b000011: begin // JAL
                // regDst = 1'b;
                selPC4 = 1'b1;
                sel31w = 1'b1;
                // ALUsrc = 1'b;
                // ALUop = 3'b;
                regWrite = 1'b1;
                memRead = 1'b0;
                memWrite = 1'b0;
                // memToReg = 1'b;
                PCsrc = PCJAL;
            end
            6'b100000: begin // JR
                // regDst = 1'b;
                // selPC4 = 1'b;
                // sel31w = 1'b;
                // ALUsrc = 1'b;
                // ALUop = 3'b0;
                regWrite = 1'b0;
                memRead = 1'b0;
                memWrite = 1'b0;
                // memToReg = 1'b;
                PCsrc = PCJR;
            end 
            default: $display("Opcode not found 404");
        endcase
    end 

    always@(func, ALUop) begin
        if(ALUop == ADD || ALUop == SUB || ALUop == AND) begin
            case (ALUop)
                ADD: ALUoperation = 3'b010;
                SUB: ALUoperation = 3'b011;
                AND: ALUoperation = 3'b000; 
            endcase
        end
        else if(ALUop == RT) begin
            case (func)
                6'b100000: ALUoperation = 3'b010; // ADD
                6'b100100: ALUoperation = 3'b000; // AND
                6'b100010: ALUoperation = 3'b011; // SUB
                6'b100101: ALUoperation = 3'b001; // OR
                6'b101010: ALUoperation = 3'b100; // SLT
                default:begin
                            {regDst, selPC4, sel31w, ALUsrc, regWrite, memRead, memWrite, memToReg} = 8'b0;
                            {selPC, PCb, PCjal, PCjr} = 4'b0000;
                            $display("nothing achived");
                    end
            endcase
        end
    end

    always@(PCsrc, zero) begin
        {selPC, PCb, PCjal, PCjr} = 4'b0000;
        if(PCsrc == SELPC)begin
            selPC = 1'b1;
        end
        else if(PCsrc == PCBEQ)begin
            PCb = zero;
            selPC = ~zero;
        end
        else if(PCsrc == PCBNQ)begin
            PCb = ~zero;
            selPC = zero;
        end
        else if(PCsrc == PCJAL)begin
            PCjal = 1'b1;
        end
        else if(PCsrc == PCJR)begin
            PCjr = 1'b1;
        end
    end

endmodule
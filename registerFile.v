module Register_file(input clk, rst, reg_write, input [4:0] read_register1, input[4:0] read_register2, input[4:0] write_register, input[31:0] write_data, output reg [31:0] read_data1, output reg [31:0] read_data2);
    reg [31:0] Reg [0:31];
    integer i;
    initial begin
        Reg[0] = 32'b0;
    end
    always@(negedge clk, posedge rst) begin
        if(rst == 1) begin
            for(i = 0; i < 32; i = i + 1) begin
                Reg[i] = 32'b0;
            end         
        end
        else begin
            if(reg_write) begin
                Reg[write_register] = write_data;
            end
        end
        Reg[0] = 32'b0;
    end
    always@(read_register1, read_register2, Reg) begin
        read_data1 = Reg[read_register1];
        read_data2 = Reg[read_register2];
    end
endmodule
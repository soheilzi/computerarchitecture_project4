module TBALU();
  reg[31:0] A;
  reg[31:0] B;
  reg[2:0] op;
  wire[31:0]C;
  wire zero;
  ALU U1 (A, B, op, zero, C);
  initial begin
    A = 32'b 1101100;
    B = 32'b 11;
    op = 3'b 100;
    #500
    A = 32'b 0;
    #500
    $stop;
  end
endmodule
    

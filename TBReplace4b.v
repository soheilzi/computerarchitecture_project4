module TBReplace4b();
  reg[31:0] a;
  reg[3:0] b;
  wire [31:0] c;
  Replace4b U1 (a, b, c);
  initial begin
    a = 32'b 0;
    b = 4'b 1111;
    #500
    $stop;
  end
endmodule
  

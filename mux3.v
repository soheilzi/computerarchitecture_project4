module Mux3(input selpc4, seljump, selbeq, input [31:0] addOut, comOut, PC4, output reg [31:0]newPC);
    always@(selpc4, seljump, selbeq, addOut, comOut, PC4)begin
        newPC = selpc4;
        if(selpc4)begin
            newPC = PC4;
        end
        if(seljump)begin
            newPC = comOut;
        end
        if(selbeq)begin
            newPC = addOut;
        end
    end
endmodule
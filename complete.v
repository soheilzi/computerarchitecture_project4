module complete(input clk, rst);
  wire RegDst;
  wire SelPC4;
  wire Sel31w;
  wire ALUsrc;
  wire RegWrite;
  wire MemRead;
  wire MemWrite;
  wire MemtoReg;
  wire SelPC;
  wire PCb;
  wire PCjal;
  wire PCjr;
  wire [2:0]ALUoperation;
  wire zero;
  wire [5:0]opc;
  wire [5:0]func;
  
  
  
  DataPath DP ( .clk(clk), .rst(rst), .RegDst(RegDst), .SelPC4(SelPC4), .Sel31w(Sel31w), .ALUsrc(ALUsrc), .RegWrite(RegWrite), .MemRead(MemRead), .MemWrite(MemWrite), .MemtoReg(MemtoReg), .SelPC(SelPC), .PCb(PCb), .PCjal(PCjal), .PCjr(PCjr), .ALUoperation(ALUoperation), .zero(zero), .opc(opc), .func(func));

  Controller cntrl ( .OPC(opc), .func(func), .zero(zero), .regDst(RegDst), .selPC4(SelPC4), .sel31w(Sel31w), .ALUsrc(ALUsrc), .ALUoperation(ALUoperation), .regWrite(RegWrite), .memRead(MemRead), .memWrite(MemWrite), .memToReg(MemtoReg), .selPC(SelPC), .PCb(PCb), .PCjal(PCjal), .PCjr(PCjr));
  
endmodule
module EX_MEM(input clk, rst, 
    input zero_in, input [1:0] WB_in, M_in, input [4:0] RegDst_in, input [31:0] Result_in, Data2_in,
    output reg zero_out, output reg [1:0] WB_out, M_out, output reg [4:0] RegDst_out, output reg [31:0] Result_out, Data2_out);
    always @ (posedge clk, posedge rst) begin
      if(rst == 1)
        {zero_out, WB_out, M_out, RegDst_out, Result_out, Data2_out} = 74'b 0;
      else begin
        zero_out = zero_in;
        WB_out = WB_in;
        M_out = M_in;
        RegDst_out = RegDst_in;
        Result_out = Result_in;
        Data2_out = Data2_in;
      end
    end



endmodule

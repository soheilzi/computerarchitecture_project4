module DataPath(input clk, rst, RegDst, SelPC4, Sel31w, ALUsrc, RegWrite, MemRead, MemWrite, MemtoReg, SelPC, PCb, PCjal, PCjr, input[2:0] ALUoperation, output zero, output [5:0]opc, func);
  wire [31:0] PCin;
  wire [31:0] PC;
  wire [31:0] PC4;
  wire [31:0] const4;
  wire [4:0] const31;
  wire const1;
  wire const0;
  wire [31:0] Inst;
  wire [31:0] InstMemWrite;
  wire [31:0] MAtoReg;
  wire [31:0] ALUout;
  wire [31:0] RegWriteData;
  wire [31:0] Memout;
  wire [31:0] Data1;
  wire [31:0] Data2;
  wire [31:0] SiEx_in;
  wire [31:0] ALU2in;
  wire [31:0] SiExSh;
  wire [31:0] PCbranch;
  wire [31:0] Sh_in;
  wire [31:0] PCjump;
  wire [4:0] RegD1;
  wire [4:0] RegD2;
  
  
  
  assign func = Inst[5:0];
  assign opc = Inst[31:26];
  
  assign const4 = 32'b100;
  assign const31 = 5'b11111;
  assign const1 = 1'b1;
  assign const0 = 1'b0;


  Adder32b addpc ( .A(PC), .B(const4), .C(PC4));
  Instruction_memory InstMem ( .mem_read(const1), .mem_write(const0), .clk(clk), .rst(rst), .address(PC), .write_data(const0), .instruction(Inst));
  
  MUX2to1 mux1 ( .A0(ALUout), .A1(Memout), .sel(MemtoReg), .out(MAtoReg));
  MUX2to1 mux2 ( .A0(MAtoReg), .A1(PC4), .sel(SelPC4), .out(RegWriteData));
  MUX5b mux3 ( .A0(Inst[20:16]), .A1(Inst[15:11]), .sel(RegDst), .out(RegD1));
  MUX5b mux4 ( .A0(RegD1), .A1(const31), .sel(Sel31w), .out(RegD2));
  
  Register_file RegFile ( .clk(clk), .rst(rst), .reg_write(RegWrite), .read_register1(Inst[25:21]), .read_register2(Inst[20:16]), .write_register(RegD2), .write_data(RegWriteData), .read_data1(Data1), .read_data2(Data2));
  
  SignEx SE ( .in(Inst[15:0]), .out(SiEx_in));
  
  MUX2to1 mux5 ( .A0(Data2), .A1(SiEx_in), .sel(ALUsrc), .out(ALU2in));
  ALU alu ( .A(Data1), .B(ALU2in), .operation(ALUoperation), .Zero(zero), .result(ALUout));
  
  Data_memory DataMem ( .mem_read(MemRead), .mem_write(MemWrite), .clk(clk), .rst(rst), .address(ALUout), .write_data(Data2), .read_data(Memout));
  
  ShL2 shEx ( .in(SiEx_in), .out(SiExSh));
  Adder32b addbranch ( .A(PC4), .B(SiExSh), .C(PCbranch));
  
  ShL2 shIn ( .in(Inst), .out(Sh_in));
  Replace4b rep ( .A(Sh_in), .B(PC[31:28]), .C(PCjump));
  
  MUX4to1 mux6 ( .A0(PC4), .sel0(SelPC), .A1(PCbranch), .sel1(PCb), .A2(PCjump), .sel2(PCjal), .A3(Data1), .sel3(PCjr), .out(PCin));
  
  Reg32b pcReg ( .clk(clk), .rst(rst), .in(PCin), .out(PC));
 
endmodule
  
  
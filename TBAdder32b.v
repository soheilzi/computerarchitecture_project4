module TBAdder32b();
  reg[31:0] A;
  reg[31:0] B;
  wire[31:0] C;
  Adder32b U1 (A, B, C);
  initial begin
    A = 32'b 100;
    B = 32'b 0001101100;
    #500
    $stop;
  end
endmodule
  

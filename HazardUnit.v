module HazardUnit(input clk, ID_EX_MemRead, ID_EX_RegWrite, EX_MEM_RegWrite , MEM_WB_RegWrite ,input [4:0] ID_EX_Rt, IF_ID_Rs, IF_ID_Rt, ID_EX_Rd, EX_MEM_Rd, MEM_WB_Rd, input [5:0] opc, output reg PcWrite, IF_ID_Write, SelControlSignals);

always @ (ID_EX_MemRead, ID_EX_Rt, IF_ID_Rs, IF_ID_Rt, clk) begin
  {PcWrite, IF_ID_Write, SelControlSignals} = 3'b111;
  if((opc == 6'b000100 || opc == 6'b000101) && MEM_WB_RegWrite == 1 && ((MEM_WB_Rd == IF_ID_Rs && IF_ID_Rs != 5'b0)|| (MEM_WB_Rd == IF_ID_Rt && IF_ID_Rt != 5'b0)))
    {PcWrite, IF_ID_Write, SelControlSignals} = 3'b111;
  else begin
    if((ID_EX_MemRead == 1 ) && (ID_EX_Rt == IF_ID_Rs || ID_EX_Rt == IF_ID_Rt))
      {PcWrite, IF_ID_Write, SelControlSignals} = 3'b0;
    if((opc == 6'b000100 || opc == 6'b000101) && ID_EX_RegWrite == 1 && ((ID_EX_Rd == IF_ID_Rs && IF_ID_Rs != 5'b0) || (ID_EX_Rd == IF_ID_Rt && IF_ID_Rt != 5'b0)))
      {PcWrite, IF_ID_Write, SelControlSignals} = 3'b0;
    if((opc == 6'b000100 || opc == 6'b000101) && EX_MEM_RegWrite == 1 && ((EX_MEM_Rd == IF_ID_Rs && IF_ID_Rs != 5'b0) || (EX_MEM_Rd == IF_ID_Rt && IF_ID_Rt != 5'b0)))
      {PcWrite, IF_ID_Write, SelControlSignals} = 3'b0;
  end

end


endmodule

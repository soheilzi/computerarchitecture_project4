module Replace4b(input [31:0] A, [3:0]B, output[31:0]C);
  assign C[27:0] = A[27:0];
  assign C[31:28] = B;
endmodule

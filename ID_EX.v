module ID_EX(input clk, rst,
    input [1:0] WB_in, M_in, input [4:0] EX_in, input [4:0] Rt_in, Rd_in, Rs_in, input [31:0] Data1_in, Data2_in, Extend_in,
    output reg [1:0] WB_out, M_out, output reg [4:0] EX_out, output reg [4:0] Rt_out, Rd_out, Rs_out, output reg [31:0] Data1_out, Data2_out, Extend_out);
    always @ (posedge clk, posedge rst)begin
    if(rst == 1)
        {WB_out, M_out, EX_out, Rt_out, Rd_out, Rs_out, Data1_out, Data2_out, Extend_out} = 119'b 0;
    else begin
        WB_out = WB_in;
        M_out = M_in;
        EX_out = EX_in;
        Rt_out = Rt_in;
        Rs_out= Rs_in;
        Rd_out = Rd_in;
        Data1_out = Data1_in;
        Data2_out = Data2_in;
        Extend_out = Extend_in;
    end
    end
    
endmodule

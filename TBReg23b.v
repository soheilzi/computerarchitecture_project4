module TBReg32b();
  reg[31:0] in;
  reg clk;
  reg rst;
  wire [31:0] out;
  Reg32b U1 (clk, rst, in, out);
  initial begin
    clk = 0;
    rst = 1;
    in = 32'b 1;
    #100
    clk = 1;
    #100
    rst = 0;
    clk = 0;
    #100
    clk = 1;
    #100
    $stop;
  end
endmodule

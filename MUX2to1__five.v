module MUX2to1_five(input [4:0]A0, A1, input sel, output [4:0] out);
  assign out = (sel == 1'b1) ? A1 : A0;
endmodule
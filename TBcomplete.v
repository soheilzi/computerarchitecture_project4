module TBcomplete();
  reg clk;
  reg rst;
  
  mips U1 (.clk(clk), .rst(rst));
  initial begin
    clk = 0;
    #50
    clk = 1;
    repeat(5000) begin
      #120
      clk = ~clk;
    end
    $stop;
  end
    
  initial begin
    rst = 0;
    #50
    rst = 1;
    #50 rst = 0;
    
    #100000
    $stop;
  end
endmodule

module FlushControll(output reg flush, input [31:0]instruction, input[31:0] data1, data2);
    always@(instruction, data2, data1)begin
        flush = 1'b0;
        if ((instruction[31:26] == 6'b000100) && (data1 == data2))begin
            flush = 1'b1;
        end
        if(((instruction[31:26] == 6'b000101) && (data1 != data2)))begin
            flush = 1'b1;
        end
        if (instruction[31:26] == 6'b000010)begin
            flush = 1'b1;
        end
    end
    // assign flush = ((instruction[31:26] == 6'b000100) && (eq_out == 1'b1))? 1 : ((instruction[31:26] == 6'b000101) && (eq_out == 1'b0))? 1 : (instruction[31:26] == 6'b000010) ? 1 : 0;
    // assign jump = flush;
endmodule 
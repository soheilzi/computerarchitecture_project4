module mips(input rst, clk);
    wire [31:0]inst;
    wire [31:0]IF_ID_PC;
    wire IF_ID_Write;
//IF START
    wire [31:0]const4;
    wire [31:0]pc;
    wire [31:0]pcPlus4;
    wire [31:0]instruction;
    wire [31:0]newPC;
    wire [31:0]controlledPC;
    wire flush;
    wire pcWrite;

    wire [4:0]IF_ID_Rd;
    wire [4:0]IF_ID_Rs;
    wire [4:0]IF_ID_Rt;
    wire [31:0]exOut;
    wire [31:0]shOut;
    wire [31:0]read_data1;
    wire [31:0]read_data2;
    wire [31:0]comOut;
    wire [31:0]addOut;
    wire jump;
    wire SelControlSignals;

    wire [2:0]ALUoperation;
    wire [31:0]read_data1_EX;
    wire [31:0]read_data2_EX;

    wire [31:0]extend_EX;
    wire [4:0]Rs_EX;
    wire [4:0]Rd_EX;
    wire [4:0]Rt_EX;

    wire regDst;
    wire ALUsrc;
    wire regWrite;
    wire memRead;
    wire memWrite;
    wire memToReg;
    wire selPC;

    wire [1:0]WB_EX;
    wire [1:0]M_EX;
    wire [4:0]EX;

    wire [31:0]ALU_A;
    wire [31:0]ALU_B;
    wire [31:0]mux_B_out;
    wire [31:0]result;
    wire [1:0]forwardA;
    wire [1:0]forwardB;
    wire [4:0]dest;

    wire [1:0]WB_MEM;
    wire [1:0]M_MEM;
    wire [4:0]MEM_Rd;
    wire [31:0]result_MEM;
    wire [31:0]Data2_out;

    wire [31:0]read_data;

    wire [1:0]WB_WB;
    wire [4:0]write_register;
    wire [31:0]result_WB;
    wire [31:0]data_WB;
    wire [31:0]write_data;

    wire [31:0]combShOut;

    wire selpc4;
    wire seljump;
    wire selbeq;

    wire eq_out;

    assign const4 = 4;

    Instruction_memory instMemory(.mem_read(1'b1), .mem_write(), .clk(clk), .rst(rst), .address(pc), .write_data(), .instruction(instruction));
    FlushControll flusher(.flush(flush), .instruction(inst), .data1(read_data1), .data2(read_data2));
    Reg32b PC(.clk(clk), .rst(rst), .write(pcWrite), .in(newPC), .out(pc));
    Adder32b adder4(.A(pc), .B(const4), .C(pcPlus4));
    // MUX2to1 selpcMux(.A0(pcPlus4), .A1(controlledPC), .sel(flush), .out(newPC));
    Mux3 selpcMux(.selpc4(selpc4), .seljump(seljump), .selbeq(selbeq), .addOut(addOut), .comOut(comOut), .PC4(pcPlus4), .newPC(newPC));
//IF END   
    IF_ID IF__ID(.clk(clk), .rst(rst), .IF_Flush(flush), .IF_ID_write(IF_ID_Write), .Inst_in(instruction), .PC_in(pcPlus4), .Inst_out(inst), .PC_out(IF_ID_PC));
//ID START
    
    

    assign IF_ID_Rd = inst[15:11];
    assign IF_ID_Rs = inst[25:21];
    assign IF_ID_Rt = inst[20:16];
    SignEx signExted(.in(inst[15:0]), .out(exOut));
    Register_file registerFile(.clk(clk), .rst(rst), .reg_write(WB_WB[1]), .read_register1(IF_ID_Rs), .read_register2(IF_ID_Rt),
     .write_register(write_register), .write_data(write_data), .read_data1(read_data1), .read_data2(read_data2));
    assign eq_out = (read_data2 == read_data1) ? 1 : 0;


    ShL2 shiftLeft2(.in(exOut), .out(shOut));
    ShL2 shiftLeft2_2(.in(inst), .out(combShOut));
    Replace4b combine(.A(combShOut), .B(IF_ID_PC[31:28]), .C(comOut));
    Adder32b adder(.A(IF_ID_PC), .B(shOut), .C(addOut));
    // MUX2to1 PCMux(.A0(comOut), .A1(addOut), .sel(jump), .out(controlledPC));
    
    HazardUnit hazardUnit(.MEM_WB_RegWrite(WB_WB[1]), .ID_EX_MemRead(M_EX[0]), .ID_EX_RegWrite(WB_EX[1]), .EX_MEM_RegWrite(WB_MEM[1]), .ID_EX_Rt(Rt_EX), .IF_ID_Rs(IF_ID_Rs), .IF_ID_Rt(IF_ID_Rt), .ID_EX_Rd(dest), .EX_MEM_Rd(MEM_Rd), .MEM_WB_Rd(write_register), .opc(inst[31:26]),.PcWrite(pcWrite), .IF_ID_Write(IF_ID_Write), .SelControlSignals(SelControlSignals), .clk(clk));

    
    Controller controller(.OPC(inst[31:26]), .func(inst[5:0]), .zero(eq_out), .NoP(SelControlSignals), .ALUoperation(ALUoperation), 
                    .regDst(regDst), .selPC4(), .sel31w(), .ALUsrc(ALUsrc), .regWrite(regWrite), .memRead(memRead), .memWrite(memWrite), .memToReg(memToReg),
                    .selPC(selpc4), .PCb(selbeq), .PCjal(seljump), .PCjr());

//ID END
    
    ID_EX ID__EX(.clk(clk), .rst(rst)
    , .WB_in({regWrite, memToReg}), .M_in({memWrite, memRead}), .EX_in({ALUoperation[2:0], ALUsrc, regDst})
    , .Rt_in(IF_ID_Rt), .Rd_in(IF_ID_Rd), .Rs_in(IF_ID_Rs)
    , .Data1_in(read_data1), .Data2_in(read_data2), .Extend_in(exOut)
    , .WB_out(WB_EX), .M_out(M_EX), .EX_out(EX), .Rt_out(Rt_EX), .Rd_out(Rd_EX), .Rs_out(Rs_EX), .Data1_out(read_data1_EX), .Data2_out(read_data2_EX), .Extend_out(extend_EX));
//EX START
    

    ForwardingUnit forwardingUnit(.EX_MEM_RegWrite(WB_MEM[1]), .MEM_WB_RegWrite(WB_WB[1]), .EX_MEM_Rd(MEM_Rd), .MEM_WB_Rd(write_register), .ID_EX_Rs(Rs_EX), .ID_EX_Rt(Rt_EX),
    .ForwardA(forwardA), .ForwardB(forwardB));
    MUX4to1 A_mux(.A0(read_data1_EX), .A1(result_MEM), .A2(write_data), .A3(), .sel(forwardA), .out(ALU_A));
    MUX4to1 B_mux(.A0(read_data2_EX), .A1(result_MEM), .A2(write_data), .A3(), .sel(forwardB), .out(mux_B_out));

    MUX2to1 ALUmux(.A0(mux_B_out), .A1(extend_EX), .sel(EX[1]), .out(ALU_B));
    MUX2to1_five Dstmux(.A0(Rt_EX), .A1(Rd_EX), .sel(EX[0]), .out(dest));

    ALU alu(.A(ALU_A), .B(ALU_B), .operation(EX[4:2]), .Zero(), .result(result));

//EX END
    
    EX_MEM EX__MEM(.clk(clk), .rst(rst), 
    .zero_in(), .WB_in(WB_EX), .M_in(M_EX), .RegDst_in(dest), .Result_in(result), .Data2_in(mux_B_out),
    .zero_out(), .WB_out(WB_MEM), .M_out(M_MEM), .RegDst_out(MEM_Rd), .Result_out(result_MEM), .Data2_out(Data2_out));
//MEM START
    Data_memory data_memory(.mem_read(M_MEM[0]), .mem_write(M_MEM[1]), .clk(clk), .rst(rst), .address(result_MEM), .write_data(Data2_out), .read_data(read_data));
//MEM END
    
    MEM_WB MEM__WB(.clk(clk), .rst(rst),
    .WB_in(WB_MEM), .RegDst_in(MEM_Rd), .Result_in(result_MEM), .Data2_in(read_data),
    .WB_out(WB_WB), .RegDst_out(write_register), .Result_out(result_WB), .Data2_out(data_WB));
    MUX2to1 WBMux(.A0(result_WB), .A1(data_WB), .sel(WB_WB[0]), .out(write_data));

endmodule 
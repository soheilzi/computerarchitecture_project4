module MEM_WB(input clk, rst,
    input [1:0] WB_in, input [4:0] RegDst_in, input [31:0] Result_in, Data2_in,
    output reg [1:0] WB_out, output reg [4:0] RegDst_out, output reg [31:0] Result_out, Data2_out);
    always @ (posedge clk, posedge rst)begin
      if(rst == 1)
        {WB_out, RegDst_out, Data2_out, Result_out} = 71'b 0;
      else begin
        WB_out = WB_in;
        RegDst_out = RegDst_in;
        Result_out = Result_in;
        Data2_out = Data2_in;
      end
    end
    
endmodule

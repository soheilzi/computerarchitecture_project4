module TB();
    reg [31:0]write_data;
    reg [31:0]address;
    reg clk;
    reg rst;
    reg mem_read;
    reg mem_write;

    wire [31:0] read_data;
    
    Data_memory cut(.clk(clk), .rst(rst), .mem_read(mem_read), .mem_write(mem_write),
                     .address(address), .write_data(write_data), .read_data(read_data));
    initial begin
        rst = 1'b1;
        clk = 1'b0;
        #100
        rst = 1'b0;
        repeat(100) #50 clk = ~clk;
    end

    initial begin
        #20
        mem_write = 1'b1;
        mem_read = 1'b0;
        address = 32'd20;
        write_data = 32'd18;
        #1000
        mem_write = 1'b0;
        mem_read = 1'b1;
        #1000$stop;
    end
endmodule
module TBMUX4to1();
  reg[31:0]a;
  reg[31:0]b;
  reg[31:0]c;
  reg[31:0]d;
  reg sela;
  reg selb;
  reg selc;
  reg seld;
  wire [31:0] out;
  MUX4to1 U1 (a, b, c, d, sela, selb, selc, seld, out);
  initial begin
    a = 32'b 00;
    b = 32'b 01;
    d = 32'b 11;
    sela = 1;
    selb = 0;
    selc = 0;
    seld = 0;
    #500
    sela = 0;
    selb = 1;
    #500
    selb = 0;
    selc = 1;
    #500 
    selc =  0;
    seld = 1;
    #500
    $stop;
  end
endmodule
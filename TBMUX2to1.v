module TBMUX2to1();
  reg [31:0]a;
  reg[31:0] b;
  wire[31:0]c;
  reg sel;
  MUX2to1 U1 (a, b, sel, c);
  initial begin
    sel = 0;
    a = 32'b 11111;
    b = 32'b 0;
    #500
    sel = 1;
    #500
    $stop;
  end
endmodule

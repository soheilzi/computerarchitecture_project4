module ALU(input [31:0] A, B, input [2:0] operation, output Zero, output reg [31:0] result);
  always@(A, B, operation) begin
    if(operation == 3'b 000)
      result = A & B;
    else if(operation == 3'b 001)
      result = A | B;
    else if(operation == 3'b 010)
      result = A + B;
    else if(operation == 3'b 011)
      result = A - B;
    else if(operation == 3'b 100)
      if(A < B)
        result = 32'b 1;
      else
        result = 32'b 0;
    else
      result = 32'b 0;
  end
  
  assign Zero = ( result == 32'b0 )? 1 : 0;
endmodule

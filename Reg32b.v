module Reg32b(input clk, rst, write, [31:0] in, output reg [31:0] out);
  always@(posedge rst, posedge clk) begin
    if(rst)
      out <= 32'b0;
    else
      if(write)begin
        out <= in;
      end
    end
endmodule
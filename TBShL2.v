module TBShL2();
  reg [31:0] a;
  wire [31:0] b;
  ShL2 U1 (a, b);
  initial begin
    a = 32'b 1;
    #500
    $stop;
  end
endmodule



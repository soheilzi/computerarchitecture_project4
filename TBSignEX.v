module TBSignEx();
  reg[15:0] a;
  wire[31:0] b;
  SignEx U1 (a, b);
  initial begin
    a = 16'b 0;
    #500
    a = 16'b 1111111111111111;
    #500
    $stop;
  end
endmodule
